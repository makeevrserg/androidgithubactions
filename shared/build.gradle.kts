plugins {
    id("kmm-library-convention")
}

android {
    namespace = libs.versions.project.group.get()
}
