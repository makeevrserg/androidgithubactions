package com.makeevrserg.kmmworkflow

expect val platform: String

class Greeting {
    fun greeting() = "Hello, $platform!"
}