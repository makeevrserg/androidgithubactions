plugins {
    alias(libs.plugins.kotlin.multiplatform) apply false
    alias(libs.plugins.android.library) apply false
    alias(libs.plugins.android.application) apply false
    alias(libs.plugins.kotlin.android) apply false
}
/**
 * Write data to .env file
 */
tasks.create("WriteEnv") {
    val envFile = File(rootProject.rootDir, "version.env")
    if (envFile.exists()) {
        envFile.delete()
        envFile.createNewFile()
    }
    fun appendVar(key: String, value: Any) = envFile.appendText("$key=$value\n")
    appendVar("PROJECT_VERSION_MAJOR", libs.versions.project.version.string.get())
    appendVar("PROJECT_VERSION_CODE", libs.versions.project.version.code.get())
    appendVar("PROJECT_NAME", libs.versions.project.name.get())
}