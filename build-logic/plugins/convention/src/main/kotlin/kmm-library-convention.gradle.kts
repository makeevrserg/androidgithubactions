group = libs.versions.project.group.get()
version = libs.versions.project.version.string.get()

plugins {
	id("com.android.library")
	kotlin("multiplatform")
}

kotlin {
	android()
	iosX64()
	iosArm64()
	iosSimulatorArm64()

	sourceSets {
		/* Main source sets */
		val commonMain by getting
		val androidMain by getting
		val iosMain by creating
		val iosX64Main by getting
		val iosArm64Main by getting
		val iosSimulatorArm64Main by getting

		/* Main hierarchy */
		androidMain.dependsOn(commonMain)
		iosMain.dependsOn(commonMain)
		iosX64Main.dependsOn(iosMain)
		iosArm64Main.dependsOn(iosMain)
		iosSimulatorArm64Main.dependsOn(iosMain)

		/* Test source sets */
		val commonTest by getting {
			dependencies {
				implementation(kotlin("test"))
			}
		}
		val androidUnitTest by getting
		val iosTest by creating
		val iosX64Test by getting
		val iosArm64Test by getting
		val iosSimulatorArm64Test by getting

		/* Test hierarchy */
		androidUnitTest.dependsOn(commonTest)
		iosTest.dependsOn(commonTest)
		iosX64Test.dependsOn(iosTest)
		iosArm64Test.dependsOn(iosTest)
		iosSimulatorArm64Test.dependsOn(iosTest)
	}
}

android {
	compileSdk = libs.versions.project.sdk.compile.get().toInt()
	defaultConfig {
		minSdk = libs.versions.project.sdk.min.get().toInt()
		targetSdk = libs.versions.project.sdk.target.get().toInt()
	}
	compileOptions {
		sourceCompatibility = JavaVersion.VERSION_17
		targetCompatibility = JavaVersion.VERSION_17
	}
}