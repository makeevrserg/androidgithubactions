import gradleapplication.Application.BUILD_NUMBER
import org.jetbrains.kotlin.gradle.plugin.mpp.pm20.util.archivesName

plugins {
	id("com.android.application")
	id("kotlin-android")
}

android {
	namespace = libs.versions.project.group.get()

	compileSdk = libs.versions.project.sdk.compile.get().toInt()
	defaultConfig {
		applicationId = libs.versions.project.group.get()
		minSdk = libs.versions.project.sdk.min.get().toInt()
		targetSdk = libs.versions.project.sdk.target.get().toInt()
		versionName = libs.versions.project.version.string.get()+".${BUILD_NUMBER}"
		versionCode = libs.versions.project.version.code.get().toInt()
		setProperty("archivesBaseName", "${libs.versions.project.name.get()}-${versionName}-${versionCode}")
	}
	buildTypes {
		getByName("debug") {
			isDebuggable = true
			applicationIdSuffix = ".debug"
			versionNameSuffix = "-DEBUG"
		}
	}
	compileOptions {
		sourceCompatibility = JavaVersion.VERSION_17
		targetCompatibility = JavaVersion.VERSION_17
	}
	kotlinOptions {
		jvmTarget = JavaVersion.VERSION_17.toString()
	}
}