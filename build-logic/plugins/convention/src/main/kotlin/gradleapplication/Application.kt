package gradleapplication

import org.gradle.api.Project
import java.util.Properties

/**
 * This class allows to import values from config files, later auto-increment of version can be implemented
 */
object Application {

    val Project.BUILD_NUMBER: Int
        get() = System.getenv("BUILD_NUMBER")?.toIntOrNull() ?: 0

    /**
     * Returns default [Properties] with secret keys
     */
    private fun getProperties(project: Project): Properties {
        return com.android.build.gradle.internal.cxx.configure.gradleLocalProperties(project.rootProject.rootDir)
    }

    /**
     * Returns secret value by its [path]
     */
    private fun getCredential(
        project: Project,
        path: String,
        default: String = ""
    ): String {
        val properties: Properties = getProperties(project)
        return System.getenv(path) ?: properties.getProperty(path, default)
    }
}
