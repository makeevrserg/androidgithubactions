plugins {
    `kotlin-dsl`
}
dependencies {
    implementation(libs.kotlin.gradle)
    implementation(libs.android.gradle)
    compileOnly(files(libs.javaClass.superclass.protectionDomain.codeSource.location))
}
